// import { Role } from "./role";

export class User {

    constructor(
        id, username, password, firstName, lastName, role, token
    ) {
        this.id = id;
        this.username = username; 
        this.password = password; 
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.token = token;
    }
    id;
    username;
    password;
    firstName;
    lastName;
    role;
    token;
}